# Build your own docker image

## Image

### How to build ?

```bash
docker build . -f .\Docker\Dockerfile -t your-image-name
```

## How to start

### With CLI (default)

```bash
docker run -p 888:80 image-name
```

- Next : Visit [http://localhost:888](http://localhost:888)

By default, if you don't bind any volume, you should see the phpinfo() home page.

### With CLI (With bind volume)

// Todo

### With docker-compose

- First, just create your docker compose by binding ports (Actually 80), volumes, image and container name (optionnal).

For more informations about docker-compose file, just check [this docker-compose file](docker-compose.yml)

To run with docker-compose, just use

```bash
docker-compose up -d
```

- -d option is optionnal

## Publish your docker image

- First, replace `public` folder by your own project,
- Build you image
- And publish it on [DockerHub](https://hub.docker.com/)

To Publish your docker image you should use

```bash
docker push your_name/your_project_name:tagname
```
